---
layout: post
title: "일상영어회화표현모음 오늘부터 해외여행가서 바로 써먹는 왕초보표현들 50문장"
toc: true
---

 일상영어회화표현모음 오늘부터 해외여행가서 방금 써먹는 왕초보표현들 50문장
 안녕하세요. 오늘은

 일상영어회화표현모음에 관한 포스팅입니다.
 오늘부터 해외여행가서 똑바로 써먹는 초보표현들 50문장에 대해서
 알려드리려고 합니다!

 ​
 관계 문장들은 왕초보자분들이

 쉽게 암기하고 사용을 할 명 있는 분들만 구성해봤습니다!
 ​
 How's it going? - 어떻게 지내?
 How's it going? I haven't seen you in a while.
 어떻게 지내? 오랫동안 못 봤네.
 ​
 What's up? - 무슨 일이야?
 Hey, what's up? Do you want to grab a coffee?
 무슨 일이야? 커피 마실래?
 ​
 Long time no see! - 오랜만이야!
 Long time no see! How have you been?
 오랜만이야! 어떻게 지냈어?
 ​
 How have you been? - 어떻게 지내왔어?
 How have you been since our last meeting?
 그간 어떻게 지냈어?
 ​
 Nice to meet you. - 만나서 반가워.
 Nice to meet you. I'm Jane, by the way.
 만나서 반가워요. 저는 제인이에요.
 ​
 Have a good day! - 좋은 날 되세요!
 Thanks for your help! Have a good day!
 도와주셔서 감사합니다! 좋은 일일 되세요!
 ​
 Take care! - 세밀히 지내!
 It's getting late, I should go. Take care!
 늦었네요, 가야겠어요. 무게 지내세요!
 See you soon! - 조만에 보자!
 I have to run now. See you soon!
 이제야 가야 해요. 대뜸 봐요!
 ​
 What do you think? - 어떻게 생각해?
 I'm planning to redecorate my room. What do you think?
 방을 새삼 꾸밀까 하는데, 어떻게 생각해?
 ​
 I agree. - 동의해.
 That's a great point. I agree with you.
 좋은 점이야. 동의해.
 ​
 I don't think so. - 그만치 생각하지 않아.
 I don't think so. There might be a better solution.
 그다지 생각하지 않아. 보다 좋은 해결책이 있을 거야.
 ​
 That's a good idea. - 좋은 생각이야.
 How about having a picnic this weekend? That's a good idea.
 차회 주말에 야유회 갈까? 좋은 생각이야.
 ​
 I'm sorry to hear that. - 안타깝네요.
 You missed the concert? I'm sorry to hear that.
 콘서트를 놓쳤다고? 안타깝네요.
 ​
 Excuse me. - 실례합니다.
 Excuse me, do you know where the nearest bank is?
 실례합니다, 근처에 은행이 어디에 있는지 아세요?
 ​
 Could you help me? - 도와줄 성명 있나요?
 Could you help me carry these boxes?
 플러스 상자들 소경 옮기는 거 도와줄 복수 있나요?
 ​
 How much is this? - 근거지 얼마에요?
 How much is this sweater?
 이 스웨터 얼마에요?
 표현을 실지 사용해보면 단순히 책이나 앱에서 공부하는 것보다

 더 빠르고 효과적으로 익힐 명 있습니다.
 따라서 실지 영어회화연습도 가능하고 외국인친구들을 사귀면서

 자연스럽게 영어회화를 공부하는 방법이 낭군 재밌지만 효과적이고 언어공부를 할 명 있다고 생각합니다.
 ​
 그래서 추천드리는 방법은 "언어교환101"

 영어회화스터디모임에 참석해서 실지로 원어민들이 사용하는

 표현들이나 단어들을 실지로 경험을 통해 자연스럽게 익히는 방법입니다.
 ​
 영어회화스터디모임에 대해서

 추가적으로 궁금한 부분이 있다면

 사이트 링크 남겨드리니 확인해보시길 바랍니다!
 ​
 언어교환101 사이트 & 카카오톡채널
 www.101language.co.kr
 한국에서 즐기는 국내어학연수 "언어교환101"
 www.101language.co.kr
 https://pf.kakao.com/_xbMxiVxb
 국내에서 즐기는 어학연수 "언어교환101" 오프라인/온라인모임 진행중
 pf.kakao.com
 Where is the restroom? - 화장실 어디에요?
 Excuse me, where is the restroom?
 실례합니다, 화장실 어디에요?
 ​
 I'm looking for... - ...을 찾고 있어요.
 I'm looking for a new pair of shoes.
 새 신발을 찾고 있어요.
 ​
 Can I have the check, please? - 계산서 주세요.
 Can I have the check, please? We need to leave soon.
 계산서 주세요. 즉금 떠나야 해요.
 ​
 Do you have any recommendations? - 추천할 만한 것 있나요?
 Do you have any recommendations for a good Italian restaurant?
 좋은 이탈리안 식당 추천해 주실 생령 있나요?
 ​
 I'm just browsing. - 가만히 둘러보는 중이에요.
 No, thank you. I'm just browsing.
 아니요, 감사합니다. 똑같이 둘러보는 중이에요.
 ​
 I'll take it. - 이걸로 할게요.
 This jacket looks great. I'll take it.
 플러스 재킷 멋지네요. 이걸로 할게요.
 ​
 Can I try this on? - 이자 입어봐도 되나요?
 Can I try this on? It looks like my size.
 터전 입어봐도 되나요? 타이틀 사이즈인 것 같아요.
 ​
 What time is it? - 몇 시에요?
 What time is it? I need to catch the bus.
 몇 시에요? 버스를 타야 해요.
 ​
 I'm hungry. - 배고파요.
 Do you want to get lunch? I'm hungry.
 점심 먹을래? 배고파.
 ​
 I'm thirsty. - 목말라요.
 Can we stop for a drink? I'm thirsty.
 음료 오죽 마실까? 목말라.
 ​
 Can I get a refill? - 리필해 주세요.
 Excuse me, can I get a refill on my coffee?
 실례합니다, 커피 리필해 주실 명 있나요?
 ​
 I'm tired. - 피곤해요.
 It's been a long day. I'm tired.
 각하 하루 힘들었어. 피곤해.
 ​
 I need some rest. - 좀 쉬어야겠어요.
 After the hike, I need some rest.
 하이킹 후에 여북 쉬어야겠어요.
 Let's go! - 가자!
 The movie starts in ten minutes. Let's go!
 영화가 10분 후에 시작해. 가자!
 ​
 I'm sorry. - 미안해요.
 I'm sorry for being late.
 늦어서 미안해요.
 ​
 Thank you. - 감사합니다.
 Thank you for your help.
 도와주셔서 감사합니다.
 ​
 You're welcome. - 천만에요.
 You're welcome! I'm glad I could help.
 천만에요! 도와드릴 생목숨 있어서 기뻐요.
 ​
 No problem. - 문제없어요.
 Can you help me move this? Sure, no problem.
 계기 옮기는 거 도와줄 수 있나요? 물론, 문제없어요.
 ​
 I don't understand. - 이해가 여인 돼요.
 Can you explain that again? I don't understand.
 새로이 설명해 주시겠어요? 이해가 여자 돼요.
 ​
 Could you repeat that? - 새로이 말씀해 주시겠어요?
 Sorry, could you repeat that?
 죄송한데, 거듭 말씀해 주시겠어요?
 ​
 What does that mean? - 그게 무슨 뜻이에요?
 What does that word mean?
 그쪽 단어가 무슨 뜻이에요?
 ​
 How do you say this in English? - 이걸 영어로 어떻게 말해요?
 How do you say "고양이" in English?
 "고양이"를 영어로 어떻게 말해요?
 ​
 I'm lost. - 길을 잃었어요.
 I'm lost. Can you help me find my way?
 길을 잃었어요. 길을 찾는 데 도와줄 요체 있나요?
 ​
 Where are you from? - 어디 출신이에요?
 Where are you from? I'm from Korea.
 어디 출신이에요? 저는 한국 출신이에요.
 ​
 How old are you? - 몇 살이에요?
 How old are you, if you don't mind me asking?
 몇 살이에요, 괜찮으시다면요?
 ​
 What do you do? - 무슨 작전 하세요?
 What do you do for a living?
 무슨 의전 하세요?
 ​
 Do you speak English? - 영어 하세요?
 Do you speak English? I need some help.
 영어 하세요? 도움이 필요해요.
 ​
 Can I ask you a question? - 질문해도 되나요?
 Can I ask you a question about your job?
 당신의 일에 대해 질문해도 되나요?
 ​
 How can I get to...? - ...에 어떻게 가요?
 How can I get to the nearest subway station?
 남자 가까운 지하철역에 어떻게 가요?
 ​
 Is it far? - 멀어요?
 Is it far from here?
 여기서 멀어요?
 영어회화스터디모임을 통해서

 얻을 수명 있는 부분

 ​
 실지 발음과 억양:
 외국인과 대화하면서 실지로 발음과 억양을 익힐 복 있습니다. 이는 학습자의 영어 발음과 억양을 자연스럽게 만들어 줍니다.
 ​
 제도 이해:
 외국인과의 대화를 통해 그들의 문화와 사고방식을 이해할 생명 있습니다. 이는 언어를 배우는 데 있어서 중요한 부분입니다.
 ​
 확신 향상:
 반복적인 사용을 통해 영어로 말하는 것에 대한 자신감을 얻을 성명 있습니다. 실수해도 괜찮다는 것을 배우고, 조금씩 우극 편안해질 고갱이 있습니다.
 ​
 ​
 What's your favorite...? - 좋아하는 ...은 무엇이에요?
 What's your favorite food?
 좋아하는 음식은 무엇이에요?
 ​
 Can you show me? - 보여줄 핵 있어요?
 Can you show me how to use this app?
 보탬 앱 사용하는 재주 시각 보여줄 목숨 있어요?
 ​
 Can I join you? - 같이 해도 될까요?
 Can I join you for lunch?
 점심 다름없이 해도 될까요?
 구체적인 팁
 ​
 스터디 집회 적극 참여:
 가능한 극한 빈번히 스터디 모임에 참여하세요. 일주일에 두 순차 귀결 모임에 참석하면 큰 도움이 됩니다.
 ​
 다양한 재현 사용:
 위에서 배운 표현들을 상황에 맞게 다양하게 사용해 보세요. 새로운 표현을 시도해 보는 것도 좋습니다.
 ​
 역할극(Role Play):
 역할극을 통해 다양한 상황을 연습해보세요. 예를 들어, 식당에서 주문하기, 간극 묻기, 쇼핑하기 등의 상황을 설정해 연습해 보세요.
 ​
 피드백 받기:
 이방인 친구나 스터디 모임의 다른 멤버들에게 피드백을 요청하세요. 발음, 억양, 문법 등에 대한 피드백은 큰 도움이 됩니다.
 ​
 날씨도 쓰기:
 영어로 일기를 써보세요. 일상에서 사용한 표현들을 복습하고, 새로운 표현을 연습할 핵심 있습니다.
 ​
 군 녹음:
 자신의 목소리를 녹음하고 들어보세요. 발음이나 억양을 확인하고 개선할 수명 있습니다.
 ​
 ​
 That's interesting. - 흥미롭네요.
 That's interesting. Tell me more about it.
 흥미롭네요. 더한층 이야기해 주세요.
 ​
 How much time do we have? - 작히나 시간이 있어요?
 How much time do we have before the meeting starts?
 의문 착수 전까지 여북 시간이 있어요?
 ​
 Do you need any help? - 도움이 필요해요?
 Do you need any help with your project?
 프로젝트에 도움이 필요해요?
 ​
 I'm busy. - 바빠요.
 I'm busy right now. Can we talk later?
 시재 바빠요. 나중에 이야기할 생령 있을까요?
 ​
 I like it. - 좋아요.
 I like this song. It's really catchy.
 이문 어음 좋아요. 참 중독성 있어요.
 ​
 I don't like it. - 별로에요.
 I don't like this movie. It's too slow.
 치아 영화 별로에요. 완전히 느려요.
 ​
 I have a question. - 질문이 있어요.
 I have a question about the assignment.
 과제에 대해 질문이 있어요.
 ​
 Can I use your phone? [주소모음](https://attention-collects.com/life/post-00107.html) - 전화기 단시간 써도 될까요?
 My phone is dead. Can I use your phone?
 제호 전화기가 꺼졌어요. 전화기 시각 써도 될까요?
 ​
 When is the meeting? - 회의가 언제에요?
 When is the meeting scheduled?
 회의가 언제로 예정되어 있어요?
 ​
 I'm not sure. - 깊숙이 모르겠어요.
 I'm not sure about the answer. Let me check.
 답을 빈번히 모르겠어요. 확인해 볼게요.
 ​
 I forgot. - 잊어버렸어요.
 I forgot my keys at home.
 열쇠를 집에 두고 왔어요.
 ​
 ​
